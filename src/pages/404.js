import * as React from 'react';
import Layout from '../components/layout';

const NotFoundPage = () => <Layout>404: NOT FOUND</Layout>;

export default NotFoundPage;
