import * as React from 'react';

const Nav = () => (
  <nav className="fixed w100 b0 l0 r0 flex jc-center wrap p">
    {/* <a href="#contact">Contact</a> */}
    <a href="#intro">Intro</a>
    <a href="#prestations">Prestations</a>
    <a href="#biographie">Bio</a>
    <a href="#references">Références</a>
    <a href="#contact">Contact</a>
  </nav>
);
export default Nav;
