---
templateKey: metadata
title: Flore
subtitle: Traiteur sur mesure et écoresponsable, cheffe à domicile
description: La cuisine de Flore est gourmande, fraîche et colorée.
  L'exploration des potentiels du végétal, du gluten free et des saisons y sont
  sources de créativité. Élaborée à partir de produits ultra-frais, issus de
  l'agriculture biologique ou raisonnée, tout y est fait maison et sur mesure.
image: img_9784.jpg
logo: logo.jpg
instagram: https://www.instagram.com/floregranboulan/
facebook: https://www.facebook.com/Flore-Granboulan-Cheffe-1929323917195601/
tel: 07 68 68 65 01
email: floregranboulan@gmail.com
address: "SARL Flore Traiteur, 118-130 avenue Jean Jaurès, 75169 Paris "
cgv: cgv-flore-traiteur.pdf
---
