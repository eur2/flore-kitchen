---
templateKey: references
title: Références
subtitle: xxx
logos:
  - image: ref-1.png
  - image: ref-2.jpg
  - image: ref-2.png
  - image: philharmonie.jpg
  - image: marque-puissante-strategie-creation-valeur-accenture-201904.jpg
  - image: logo-credit-agricole-png.png
  - image: images_004.png
  - image: la-villette.png
  - image: palaisdetokyo_share.png
  - image: logo-vuitton.png
  - image: carreau-du-temple.png
  - image: pictet.png
  - image: sorbonne.png
image: img_9776.jpg
---
*"*\[...] *Objectif affiché des Pères Populaires: rendre l'ordinaire extra, à prix cafète! Ce que Flore Granboulan s'applique à faire avec brio en proposant en semaine des menus dej de haute volée* \[...]" Julie Gerbet au sujet des Pères populaires - Le Fooding 2016

*"Super expérience avec Flore qui a préparé un buffet très varié, original et délicieux pour notre fête d'entreprise! Flore a été très sympathique, efficace et à l’écoute pour l’organisation de notre réception. Nous avons été très heureux de constater de plus qu'aucun plastique n’avait été utilisé"* Alix G. Adamantia - Cocktail pour 50 personnes

*"Notre expérience avec a été parfaite : le diner était original et absolument délicieux. Flore a su trouver le bon équilibre entre discrétion et spontanéité... Nous avons passé une très bonne soirée, et nous recommandons Flore sans aucune hésitation !"* Alexandre de S. - Repas à domicile
